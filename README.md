![N|Solid](https://s1.postimg.org/71nq8nv93j/android-icon-96x96.png)
# Hackamonth: Tram

The Tram application is a simple web application hacked together as a part of the Aller Finland Hackamonth in September of 2017.

##### Features:

  - Simple UI displaying when next tram leaves the office
  - Mobile friendly, 100% responsive
  - Automatically refreshed every 15 secs

##### Installation:

Clone the GIT repository like this:

```sh
cd /the/location/of/your/choice
git clone https://thomasdjupsjo@bitbucket.org/allermedia/fi-hackmonth-tram.git
```

##### Next steps:

 - Share the app to the employees
 - Integrate HSL GraphQL Api

##### License & Author
Aller Media Oy &copy;
Thomas Djupsjö